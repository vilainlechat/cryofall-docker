This is an extended image using the official one provided by AtomicTorchStudio (https://hub.docker.com/r/atomictorch/cryofall-server)

It has an extra package (screen) and run as user instead of root for a better data management.

Also a small script to send admin commands "more easily" to the game server

# Setup

### Build image


```
docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) -t cryofall ./docker
```

### Add servers in docker-compose.yml


```
server-pvp:
  image: cryofall

  stdin_open: true
  tty: true

  volumes:
    - ./Data/Pvp:/app/Data

  ports:
    - "6000:6000/udp"

```

The important part is setting volumes for each servers
`- ./Data/Pvp:/app/Data`


### Run

```
docker-compose up -d
```

Now you will have the `Data` folder with current user permissions and can easily edit your `Data/Pvp/SettingsServer.xml`

For first start, you may want to setup port if multiple servers then restart your servers with

```
docker-compose restart
```

### Attach server console

```
docker-compose exec server-pvp console
```

Detach by using `CTRL+A+D`

### Send admin commands to the server

```
docker-compose exec server-pvp admin "player.addLP 100 m4t"

and so on
```

To stop/restart the servers, don't use the docker compose restart or stop, send stop command to the game server :

```
docker-compose exec server-pvp admin "stop 60 Server maintenance"

...

docker-compose up -d
```
